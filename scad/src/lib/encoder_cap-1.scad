difference() {
    union() {
        difference() {
            cylinder(13,6,6, $fn=60);
            translate([0,0,-1]) color("red") cylinder(12, 4, 4, $fn=50);
        }
        translate([0,0,2])cylinder(11,4,4, $fn=50);
    }
    translate([0,0,-1]) color("yellow") cylinder(11, 3.1, 3, $fn=50);
}
