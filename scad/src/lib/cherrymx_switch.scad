
module switch_hole() {
    // “Negative” hole for MX Swiches. Use it as as
    // subtrahend in a difference operation.
    //
    // The hole at z=0 is the level on which the MX
    // rim hits the plate. The hole can be further
    // lowered, a bigger hole having space for the
    // rim is above z=0. The hole can easily be lowered
    // by 0.5mm.
    //
    // Regarding x and y, the hole starts at 0,0 and
    // goes until 16,16.
    //
    // The inner hole is 14mm^2.
    // The basic cutaway around is 16mm^2.
    // The gap in the MX rim is 11mm long
    //
    // BTW: The actual width of the switch (at rim) is likely specified as 15.6mm.
    //
    translate([16,0,0]) rotate(90) translate([1, 1, -11.5]) {
        color("red") translate ([0,0,0.1]) cube([14,14,21.3]);
        translate([-1,-1,0]) {
            color("yellow") {
                // lower cut-away
                translate([0,5.5,0]) cube([16,5,10]);
                // upper cut-away
                difference () {
                    translate([-0.2, -0.2, 11.5]) cube([16.4,16.4,10]);
                    translate([3,-1,11]) cube([10,18,10]);                
                }

            }
        }   
    }
}

switch_hole();
