module plate_bolt(){
    translate([0,0,-9]) {
        difference() {
            color("blue") cube([4,4,9]);
            translate([2,2,-0.1]) color("yellow") cylinder(5,1,1, $fn=20);
        }
    }
}
plate_bolt();
