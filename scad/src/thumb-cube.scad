include <../parameters.scad>
use <thumb_iface.scad>
use <terminal.scad>
use <lib/screw_flat_head.scad>
use <lib/cherrymx_switch.scad>
use <lib/nut.scad>



tc_depth = key_width + border + screw_bolt_d;
tc_width = 2*border + 4*key_width;
tc_z_offset = -5 - plate_thickness; // z value of the plantes bottom
y_offset = -tc_depth + column_offsets[4] + key_width;
x_offset = 2*border + 4*key_width;
x_right_end = x_offset + tc_width;

_screw_3_x = border + 6*key_width + 1*screw_bolt_d;
_screw_4_x = x_right_end - screw_bolt_d/2;

tc_screws = [
    // uper row, left to right
    [[x_offset + 1*screw_bolt_d + 3, column_offsets[4] + key_width - screw_bolt_d/2], true],
    [[_screw_3_x - 1, column_offsets[4] + key_width - screw_bolt_d/2], true],
    [[_screw_3_x + (_screw_4_x - _screw_3_x)/2, column_offsets[4] + key_width - screw_bolt_d/2], false],
    [[_screw_4_x + screw_bolt_d/2, column_offsets[4] + key_width - screw_bolt_d/2], false], 
    // within bridge circle
    [[x_offset - screw_bolt_d/2 - 3.9, y_offset + 0.85*screw_bolt_d], true],
    // lower row, left to right
    [[x_offset + screw_bolt_d/2, y_offset], true],
    [[x_offset + (x_right_end - x_offset)/2, y_offset], true],
    [[x_right_end - screw_bolt_d/2, y_offset], false]
];

tc_height = tc_inner_height + bottom_thickness + wall_nudge_height;

stand_cylinder_length = 5;
stand_plate_height = 4;
stand_block_width = 9;

function thumb_case_screw_length() = tc_inner_height + bottom_thickness + plate_thickness;

module tc_stand() {
    color("pink")
    difference() {
        union() {
            for (i = tc_screws) {
                if (! i[1]) {
                    translate([0, 0, tc_z_offset - tc_height - stand_cylinder_length + wall_nudge_height])
                        translate(i[0]) {
                            cylinder(d=screw_bolt_d, h=stand_cylinder_length, $fn=screw_bolt_fn);
                        }
                }
            }
            xl = tc_screws[3][0].x - tc_screws[2][0].x;
            yl = tc_screws[3][0].y - tc_screws[7][0].y;
            echo(yl);
            translate([0, 0, tc_z_offset - tc_height - stand_plate_height + wall_nudge_height]) {
                translate(tc_screws[2][0])
                    translate([0, -screw_bolt_d/2, 0])
                        cube([xl, screw_bolt_d, stand_plate_height]);
                translate(tc_screws[7][0])
                    translate([-screw_bolt_d/2, 0, 0])
                        cube([screw_bolt_d, yl, stand_plate_height]);
                translate(tc_screws[7][0])
                   translate([-stand_block_width - screw_bolt_d/2 + 0.5, 0, -30 + stand_plate_height])
                        cube([stand_block_width, yl + screw_bolt_d/2, 30]);
            }
        }
        for (i = tc_screws) {
            if (! i[1]) {
                translate([0, 0, tc_z_offset - tc_height - stand_cylinder_length + wall_nudge_height]) {
                    translate(i[0]) {
                        translate([0,0,-1])
                            nut(nut_size, nut_height + 1);
                        translate([0,0,1])
                            cylinder(d=screw_d, h=stand_cylinder_length, $fn=30);
                    }
                }
            }
        }
        stand_cutaway();
    }
}

module controller_tray_passage_die() {
    translate([controller_front_center_xy.x, controller_front_center_xy.y, case_bottom_z()]) {
        translate([0, -c_tray_length, 0]) rotate(270,[1,0,0]) {
            color("DarkViolet") {
                translate([
                    -10,
                    wall - 1,
                    -20 
                ])
                    cube([
                        13,
                        0.7*(c_tray_base_height + c_tray_upper_height) - 2*wall + 1.4,
                        29
                    ]);
            }
        }
    }
}

module tc_outline() {
    bridge_circle_r = column_offsets[4] + key_width - screw_bolt_d/2;
    bridge_circle_c = [x_offset, y_offset + bridge_circle_r];
    difference() {
        union() {
            translate([x_offset, y_offset])
                square([tc_width, tc_depth]);
            translate(bridge_circle_c)
                circle(r=bridge_circle_r, $fn=50);
            for (point = tc_screws) {
                translate(point[0])
                    color("red") circle(d=screw_bolt_d, $fn=screw_bolt_fn);
            }
        }
        finger_plate_outline();
    }
}

module tc_switches() {
    translate([x_offset + border, y_offset + border, tc_z_offset + 1*plate_thickness - switch_indent])
        translate(switch_offset)
            for (i=[0:3]) {
                translate([i*key_width,0,0])
                    switch_hole();
            }
}

module tc_top() {
    difference() {
        translate([0, 0, tc_z_offset]) {
            difference() {
                linear_extrude(plate_thickness) tc_outline();
                // tc_switches
                translate([0,0,-1]) color("red") {
                    linear_extrude(wall_nudge_height+1) {
                        difference() {
                            offset(delta = wall_nudge_width)
                                tc_outline();  
                            offset(delta = -wall_nudge_width)
                                tc_outline();
                        }
                    }
                }
                for (i = tc_screws) {
                    translate([i[0].x, i[0].y, plate_thickness])
                        screw_flat_head(d=screw_d, dh=screw_head_d, ah=screw_head_a, l=plate_thickness+0.1, head_elongation=1);
                } 
            }

        }
        tc_switches();
    }
}

module tc_case() {
    cyl_height = tc_inner_height + wall_nudge_height;
    encoder_x_shift=8.8;
    difference() {
        union() {
            color("SteelBlue") thumb_connectors();
            translate([0, 0, -tc_height + tc_z_offset + wall_nudge_height]) {
                difference() {
                    color("SteelBlue") linear_extrude(tc_height)
                        tc_outline();
                    translate([0, 0, bottom_thickness])
                        linear_extrude(tc_height)
                            offset(delta=-wall) tc_outline();

                }
                for (i = tc_screws) color("SeaGreen") translate([0, 0, bottom_thickness]) {
                    translate(i[0])
                        cylinder(h=tc_inner_height, d=screw_bolt_d, $fn=screw_bolt_fn);
                }
            }
        }
        tc_top();
        for (i = tc_screws) color("SteelBlue") translate([0, 0, tc_z_offset - tc_height + 0.1]) {
            translate(i[0])
                cylinder(h=tc_height, d=screw_d, $fn=20);
        }
        translate([x_right_end - encoder_x_shift -screw_bolt_d, y_offset + tc_depth - wall, tc_z_offset - 14.5]) {
            rotate(-90, [1,0,0]) rotate(-90,[0,0,1]) 
                color("pink") translate([7.5,7.5,-5]) cylinder(h=16, d=encoder_hole_d, $fn=30);
        }
        translate([x_right_end - encoder_x_shift -screw_bolt_d, y_offset + tc_depth - wall, tc_z_offset - 14.5])
            rotate(-90, [1,0,0]) rotate(-90,[0,0,1]) translate([0,0,-5]) cube([14,14.3,5]);
        color("Fuchsia") {
            for (i = tc_screws) {
                if (i[1]) {
                    translate(i[0]) translate([0,0,tc_z_offset - tc_height + wall_nudge_height -1])
                        nut(nut_size, nut_height + 1);
                }
            }
        }
        // thumb passage cube cut-away
        controller_tray_passage_die();
    }
}

module thumb_cluster() {
    tc_top();
    tc_case();
    tc_stand();
}

thumb_cluster();
