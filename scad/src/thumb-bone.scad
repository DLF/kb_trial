include <../parameters.scad>
use <lib/cherrymx_switch.scad>

thumb_cluster_z_offset = 5;
thumb_bone_thickness = 16;

module thumb_switches() {

    translate(thumb_circle_center)
        for (degrees = [-6, -18, -30, -42]) {
            rotate(degrees, [0,0,1])
                translate([-switch_width/2, key_circle_r, 0])
                    switch_hole();
        }
}

module thumb_plate_outline() {
    outer_r = key_circle_r + 22;
    inner_r = key_circle_r - 5;
    difference() {
        translate(thumb_circle_center) {
            intersection() {
                difference() {
                    circle(outer_r, $fn=100);
                    circle(inner_r, $fn=100);
                }
                square(outer_r+1);
                color("green") translate([15,0]) rotate(57) square(outer_r+1);
            }
        }
    }
}

module thumb_plate() {
    translate([0,0,-7]) {
        difference() {
            translate([0,0,-plate_thickness])
                linear_extrude(plate_thickness)
                    thumb_plate_outline();
            translate([0,0,-0.8])
                thumb_switches();
        }
    }
}

module thumb_bone_outer_dice() {
    translate([0,0,-50])
        linear_extrude(50)
            thumb_plate_outline();
}

module thumb_bone_inner_dice() {
    translate([0,0,-50])
        linear_extrude(50)
            offset(delta=-wall) thumb_plate_outline();
}

module thumb_bone_nodge_dice() {
    translate([0,0,-50])
        linear_extrude(50)
            offset(delta=-wall_nudge_width) thumb_plate_outline();
}

module upper_sphere() {
    _sphere_shell(thumb_sphere_d);
}

module lower_sphere() {
    _sphere_shell(thumb_sphere_d - 2*thumb_bone_thickness);
}

module bottom_sphere() {
    _sphere_shell(thumb_sphere_d - 2*thumb_bone_thickness + 2*bottom_thickness);
}

module plate_sphere() {
    _sphere_shell(thumb_sphere_d - 2*plate_thickness);
}

module nodge_sphere() {
    _sphere_shell(thumb_sphere_d - 2*thumb_bone_thickness + 2*bottom_thickness + 2*wall_nudge_height);
}

module _sphere_shell(d) {
    sphere_center = [2*border + 4*key_width, key_width + column_offsets[4], -0*thumb_cluster_z_offset - thumb_sphere_d/2];
    translate(sphere_center) {
        sphere(d=d, $fn=100);
    }
}

module thumb_bone_bottom() {
    // bottom plate
    intersection() {
        difference() {
            bottom_sphere();
            lower_sphere();
        }
        thumb_bone_outer_dice();
    }
    intersection() {
        difference() {
            nodge_sphere();
            bottom_sphere();
            thumb_bone_inner_dice();
        }
        thumb_bone_nodge_dice();
    }
}

module thumb_bone() {
    difference() {
        // bone body
        intersection() {
            difference() {
                upper_sphere();
                bottom_sphere();
            }
            thumb_bone_outer_dice();
        }
        // inner cut-away
        intersection() {
            thumb_bone_inner_dice();
            plate_sphere();
        }
        color("red") thumb_bone_bottom();
    }
}

module thumb_cluster() {
    //thumb_plate();
    //thumb_plate_outline();
    //upper_sphere_shell();
    //thumb_bone_outer_dice();
    thumb_bone();
    translate([0,0,-5])    thumb_bone_bottom();
}


translate([-2*border - 4*key_width, 0, 0])
    thumb_cluster();

    
