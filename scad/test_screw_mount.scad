use <src/lib/nut.scad>
use <src/lib/screw_flat_head.scad>
include <parameters.scad>

height = 5;

module test() {
    difference() {
        union() {
            translate([0,0,1])
                cube([2*screw_bolt_d + 2, screw_bolt_d + 2, 2], center=true);
            translate([screw_bolt_d/2,0,0])
                cylinder(h=height, d=screw_bolt_d, $fn=screw_bolt_fn);
            translate([-screw_bolt_d/2,0,0])
                cylinder(h=height, d=screw_bolt_d, $fn=screw_bolt_fn);
        }
        translate([screw_bolt_d/2, 0, 5]) {
            screw_flat_head(d=screw_d, dh=screw_head_d, ah=90, l=10, head_elongation=0.1);
        }
        translate([-screw_bolt_d/2, 0, -1]) {
            cylinder(h=height+2, d=screw_d, $fn=30);
            translate([0,0,height + 1 - nut_height])
                color("red") nut(nut_size, nut_height + 1);
        }
     }
}

test();
