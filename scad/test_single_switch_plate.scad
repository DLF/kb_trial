use <src/lib/cherrymx_switch.scad>

plate_length = 25;


difference() {
    plate();
    translate([
        plate_length/2 - 16/2,
        plate_length/2 - 16/2,
        2.5
    ]) switch_hole();
}

module plate() {
    linear_extrude(3) {
        square(plate_length,plate_length);
    }
}

