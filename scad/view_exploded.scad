use <src/terminal.scad>
use <src/thumb-cube.scad>

finger_plate();

translate([0, -30, -10])
    tc_top();

translate([0, -30, -40])
    tc_case();

translate([0,0,-10])
    controller_part();
    
translate([0,0,-20])
    controller_mount_part();

translate([0,0,-40])
    outer_case();

translate([0,0,-60])
    controller_tray();



//finger_case_outline();
//finger_switches();
//thumb_switches();
//case_connection_die();
