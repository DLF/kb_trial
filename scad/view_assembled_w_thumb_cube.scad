use <src/terminal.scad>
use <src/thumb-cube.scad>

finger_plate();
thumb_cluster();
outer_case();
controller_part();
controller_mount_part();
controller_tray();
//stand_cutaway();
