use <src/lib/cherrymx_switch.scad>
use <src/lib/plate_bolt.scad>

plate_length = 5+5+18+18;
plate_width = plate_length;
key_width = 18;

function main_keys_x_position(ix) = ix[0] * key_width;

function main_keys_y_position(ix) = ix[1] * key_width;

function main_keys_position(ix) = [
        main_keys_x_position(ix),
        main_keys_y_position(ix)
];

switch_positions = [
    for ( i = [
            for (a = [0 : 1]) for (b = [0 : 1] ) [a, b]
        ]
    ) main_keys_position(i)
];

echo (switch_positions);

module main_keys_switch_holes() {
    translate([1,1,0]) {
        union() {
            for ( pos = switch_positions )
                translate([pos[0], pos[1], 0])
                    switch_hole();
        }
    }
}

difference() {
    plate();
    translate([
        5,
        5,
        2.5
    ]) main_keys_switch_holes();
}

plate_bolt();
translate([5+18+18+5-4, 0, 0]) plate_bolt();
translate([0, 5+18+18+5-4, 0]) plate_bolt();
translate([5+18+18+5-4, 42, 0]) plate_bolt();
translate([42/2, 42/2, 0]) plate_bolt();
module plate() {
    linear_extrude(3) {
        square([plate_length, plate_width]);
    }
}

    
