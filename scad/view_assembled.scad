use <src/terminal.scad>
use <src/thumb_iface.scad>

finger_plate();
outer_case();
controller_part();
controller_mount_part();
controller_tray();
